import sdl.Window;
import h2d.Graphics;


class Obstacle {
    public var x: Float;
    public var width: Float;
    var height: Float;
    var gapHeight: Float;
    public var isBehind: Bool;
    public var nextObstacleGenerated: Bool;

    var topPipe: h2d.Bitmap;
    var bottomPipe: h2d.Bitmap;
    var customGraphics:h2d.Graphics;
    var colors:Array<Int> = [0xff543847, 0xff84aa45, 0xff84aa45, 0xffa5c65c, 0xffb5d468,0xffc4e173,
        0xffd1ec7d, 0xffdcf685, 0xffe4fd8b, 0xffdcf685, 0xffd1ec7c, 0xffc3e173,
        0xffb5d469, 0xffa4c65c, 0xff94b751, 0xff84a945, 0xff769c39,0xff689130,
        0xff5d8728, 0xff558022, 0xff558022, 0xff558022, 0xff558022, 0xff543847];

    public function new(image, scene: h2d.Scene.Scene, x: Float, width: Float, height: Float, gapHeight:Float) {
        this.x = x;
        this.width = width;
        this.height = height;
        this.gapHeight = gapHeight;
        this.isBehind = false;
        this.nextObstacleGenerated = false;
        

        this.topPipe = new h2d.Bitmap(image, scene);
        this.topPipe.rotate(Math.PI);
        this.topPipe.x = this.x + this.topPipe.tile.width;
        this.topPipe.y = this.height;

        this.bottomPipe = new h2d.Bitmap(image, scene);
        this.bottomPipe.x = this.x;
        this.bottomPipe.y = this.height + this.gapHeight;  


        this.customGraphics = new h2d.Graphics(scene);

        var bottomPipeBottom = this.height + this.gapHeight+this.bottomPipe.tile.height;
        var i: Int = 0;


        while (i < this.colors.length) {
            this.customGraphics.beginFill(this.colors[this.colors.length-i-1]);
            //this.customGraphics.drawRect(x+2+2*i, 0, 2, this.height-this.topPipe.tile.height);
            this.customGraphics.drawRect(x+2+2*i, 0, 2, this.topPipe.y);
            

            this.customGraphics.beginFill(this.colors[i]);
            this.customGraphics.drawRect(x+2+2*i, this.height + this.gapHeight+this.bottomPipe.tile.height, 2, this.customGraphics.getScene().height - this.height + this.gapHeight+this.bottomPipe.tile.height);
            this.customGraphics.endFill();
            i +=1;
        }
    }


    public function update(gameSpeed: Float) {
        this.x -= gameSpeed;
        this.topPipe.x -= gameSpeed;
        this.bottomPipe.x -= gameSpeed;
        this.customGraphics.move(-gameSpeed, 0);      
    }

    public function toString() {
		return 'Obstacle(x:${this.x}, height: ${this.height})';
	}

    public function getLeftCoord(): Float {
        return this.x;
    }

    public function getRightCoord(): Float {
        return this.x + this.width;
    }

    public function getTopCoord(): Float {
        return this.height;
    }

    public function getBottomCoord(): Float {
        return this.height + this.gapHeight;
    }

    public function checkColision(player: Player) {
        if (player.getRightX() < this.getLeftCoord() || player.getLeftX() > this.getRightCoord() 
            || (player.getTopY() > this.getTopCoord() && player.getBottomY() < this.getBottomCoord())
        ) {
            return false;
        }
        return true;
    }

    public function checkBehind(player: Player) {
        if (!this.isBehind &&  player.bm.x + player.bm.width/2 > this.getRightCoord()) { //is not Behind
            this.isBehind = true;
            return true;
        }
        return false;
        
    }

    public function remove() {
        this.bottomPipe.remove();
        this.topPipe.remove();
        this.customGraphics.remove();
    }
    
}