import h2d.Tile;

class Difficulty {

    var playerTile: h2d.Tile;
    var brokenPlayerTile: h2d.Tile;
    public var loseSong: hxd.res.Sound;
    
    var gameSpeed: Float;
    var pipeGapHeight: Float;

    public function new(playerTile: h2d.Tile, brokenPlayerTile: h2d.Tile, loseSong:hxd.res.Sound, pipeGapHeight: Float, gameSpeed: Float) {
        this.playerTile = playerTile;
        this.brokenPlayerTile = brokenPlayerTile;
        this.loseSong = loseSong;
        this.pipeGapHeight = pipeGapHeight;
        this.gameSpeed = gameSpeed;
    }

    

    public function getPlayerTile() {
        return this.playerTile;
    }
    
    public function getBrokenPlayerTile() {
        return this.brokenPlayerTile;
    }

    public function getloseSong() {
        return this.loseSong;
    }

    public function getPipeGapHeight() {
        return this.pipeGapHeight;
    }

    public function getGameSpeed() {
        return this.gameSpeed;
    }

    public function playSong(repeat: Bool = false) {
        this.loseSong.play();
    }

    public function stopSong() {
        this.loseSong.stop();
    }
}
