import h2d.col.RoundRect;
import h2d.Interactive;
import hxd.res.Sound;
import h2d.HtmlText;
import hxd.App;
import h2d.Scene;
import h2d.Tile;
import hxd.Window;
import h2d.Console;
import Player.Player;
import Obstacle.Obstacle;
import Config.Config;

class Main extends hxd.App {
	var player: Player;
	var console:Console;
	var score: Int = 0;
    var bestScore: Int = 0;
	var pointsPerObstacle = 1;
	var obstacleList:haxe.ds.List<Obstacle>;
	var isActive: Bool = true;
	var objectHeightOffset: Float = 40;
	var obstImg: h2d.Tile;
	var gameStarted: Bool;

	var difficultyEasy: Difficulty;
	var difficultyNormal: Difficulty;
	var difficultyHard: Difficulty;
	var selectedDifficulty: Difficulty;

	var bestScoreLabel:h2d.Text;
	var scoreLabel:h2d.Text;
	var gameScene: Scene;
	var startSong: hxd.res.Sound;

	var isStartScene: Bool;
	var startScene: Scene;

	override function init() {
		@:privateAccess haxe.MainLoop.add(() -> {});
		gameScene = new Scene();
		hxd.Res.initEmbed();
		engine.backgroundColor = 0x8CE6FF;
		var image = hxd.Res.background.toTile();
		this.gameStarted = false;
		this.obstacleList = new haxe.ds.List<Obstacle>();
		var width = Std.int(image.width);
		var height = Std.int(image.height);
		this.obstImg = hxd.Res.pipe.toTile();
		difficultyEasy = new Difficulty(hxd.Res.peter_easy.toTile(),
										hxd.Res.peter_easy_broken.toTile(),
										hxd.Res.easy_lose_song,
										Config.ePipeGapHeight,
										Config.eGameSpeed);

		difficultyNormal = new Difficulty(hxd.Res.peter_normal.toTile(),
										hxd.Res.peter_normal_broken.toTile(),
										hxd.Res.lose_song2,
										Config.mPipeGapHeight,
										Config.mGameSpeed);

		difficultyHard = new Difficulty(hxd.Res.peter_h.toTile(),
										hxd.Res.peter_easy_broken.toTile(),
										hxd.Res.hard_lose_song,
										Config.hPipeGapHeight,
										Config.hGameSpeed);

		selectedDifficulty = difficultyNormal;


		player = new Player(selectedDifficulty.getPlayerTile(), selectedDifficulty.getBrokenPlayerTile(),
		selectedDifficulty.getGameSpeed(), this.gameScene, width / 2, height / 2);
		this.scoreLabel = new h2d.Text(hxd.res.DefaultFont.get(), this.gameScene);
		this.bestScoreLabel = new h2d.Text(hxd.res.DefaultFont.get(), this.gameScene);
		    @:privateAccess haxe.MainLoop.add(() -> {});

		this.startSong = hxd.Res.start_song;
		this.createStartScene();
	}

	private function createTestObstacle(x, height, gap) {
		var obstImg = hxd.Res.pipe.toTile();
		this.obstacleList.add(new Obstacle(obstImg, gameScene, x, obstImg.width, height, gap));
	}

	override function update(dt:Float) {
		if (this.isActive && this.gameStarted) {
			if (player.checkPlayerInPlaybleArea()) {
				this.lose();
			}
			player.update(dt);
			for (obstacle in this.obstacleList) {
				obstacle.update(3);
			}
			this.generateObstacle();
			this.checkObstacles();
			this.removeObstacle();
		}
		this.drawScore();
	}

	static function main() {
		new Main();
	}

	function onEvent(event : hxd.Event) {
		// keyCodes
		// 38 - arrow UP
		// 32 - space
		// 97 - W

		 if (event.keyCode == 38 || event.keyCode == 32 || event.keyCode == 87) {
			if (this.gameStarted && this.isActive) {
				this.player.jump();
			} else {
				this.startGame();	
			}
		} 


		 if (event.keyCode == 27){
			this.gameScene.removeEventListener(this.onEvent);
			this.createStartScene();
			this.lose(true);
		}
	}

	private function startGame() {

		for (obstacle in this.obstacleList) {
			obstacle.remove();
			this.obstacleList.remove(obstacle);
		}
		var y = Random.float(this.objectHeightOffset, Window.getInstance().height - this.objectHeightOffset-this.player.getHeight()*this.selectedDifficulty.getPipeGapHeight());
		this.createTestObstacle(Window.getInstance().width+10, y, this.player.getHeight()*this.selectedDifficulty.getPipeGapHeight()); // works as a first obstacle		
		this.player.restart(Window.getInstance().height / 2);
		this.isActive = true;
		this.gameStarted = true;
		this.selectedDifficulty.stopSong();
	}

	
	private function drawScore() {
		scoreLabel.textColor = 0xff0000;
		scoreLabel.setScale(Config.gameScoreScaleFactor);
		scoreLabel.text = 'score: ${this.score}';
		scoreLabel.x = gameScene.width - scoreLabel.textWidth * Config.gameScoreScaleFactor;
		scoreLabel.y = 0;

		bestScoreLabel.textColor = 0xff0000;
		bestScoreLabel.setScale(Config.gameScoreScaleFactor);
		bestScoreLabel.text = 'best: ${this.bestScore}';
		bestScoreLabel.x = gameScene.width - bestScoreLabel.textWidth * Config.gameScoreScaleFactor;
		bestScoreLabel.y = scoreLabel.textHeight*1.5;		
	}

	private function checkObstacles() {
		for (obstacle in this.obstacleList) {
			// if bump
			if (obstacle.checkColision(this.player)) {
				this.lose();
			}
			// get score
			if (obstacle.checkBehind(this.player)) {
				this.score += this.pointsPerObstacle;
			}
		}
    }

	

    private function generateObstacle():Void{
		for (obstacle in this.obstacleList) {
			if (!obstacle.nextObstacleGenerated && obstacle.x + 200 <= Window.getInstance().width) {
				obstacle.nextObstacleGenerated = true;
				var distance = Random.float(Config.minGapBetweenObstacles, Config.maxGapBetweenObstacles);
				var y = Random.float(Config.verticalGapBeforeObstacleGap, Window.getInstance().height - Config.verticalGapBeforeObstacleGap-this.player.getHeight()*this.selectedDifficulty.getPipeGapHeight());
				this.obstacleList.add(new Obstacle(obstImg, gameScene, distance + obstacle.x, obstImg.width, y, this.player.getHeight()*this.selectedDifficulty.getPipeGapHeight()));
			}
		}
    }

	private function removeObstacle():Void {
		for (obstacle in this.obstacleList) {
			if (obstacle.x+obstacle.width < 0) {
				obstacle.remove();
				this.obstacleList.remove(obstacle);
			}
		}		
	}

	private function lose(playSong: Bool = false) {
		this.player.lose();
		this.isActive = false;
		if (this.bestScore < this.score) {
			this.bestScore = this.score;
		}
		this.score = 0;	
		if (!playSong) {
			this.selectedDifficulty.playSong(false);
		}
		this.isStartScene = true;
	}

	private function createStartScene(){
		this.startScene = new Scene();
		this.startScene.addEventListener(startScreenEvent);

		var middleText = "Flappy Peter";
		var  topText = "Help Peter Svalit'";
		var bottomText = 'Press W, arrow up or space\n to jump and avoid obstacles';

		//var rr = new RoundRect(200, 100, 200, 100);
		//var b1 = new Interactive(200, 100, rr);

		
		var topTextLabel = new h2d.Text(hxd.res.DefaultFont.get(), startScene);
		topTextLabel.textColor = 0x0f0f0f;
		topTextLabel.setScale(Config.startScreenScaleFactor);
		topTextLabel.text = topText;
		topTextLabel.x = gameScene.width / 2 - topTextLabel.textWidth * Config.startScreenScaleFactor/2;
		topTextLabel.y = gameScene.height *3/ 8;

		var middleTextLabel = new h2d.Text(hxd.res.DefaultFont.get(), startScene);
		middleTextLabel.textColor = 0xff0000;
		middleTextLabel.setScale(Config.gameNameScaleFactor);
		middleTextLabel.text = middleText;
		middleTextLabel.x = gameScene.width / 2 - middleTextLabel.textWidth * Config.gameNameScaleFactor /2;
		middleTextLabel.y = gameScene.height / 2;

		var bottomTextLabel = new h2d.HtmlText(hxd.res.DefaultFont.get(), startScene);
		bottomTextLabel.textColor = 0x0f0f0f;
		bottomTextLabel.setScale(Config.startScreenScaleFactor);
		bottomTextLabel.text = bottomText;
		bottomTextLabel.x = gameScene.width / 2 - bottomTextLabel.textWidth * Config.startScreenScaleFactor/2;
		bottomTextLabel.y = gameScene.height * 5 / 8;
		
		this.gameScene.addEventListener(this.startScreenEvent);
		this.isStartScene = true;
		this.setScene(this.startScene);
		this.startSong.play();
	}

	private function startScreenEvent(event : hxd.Event) {
		if (this.isStartScene && (event.keyCode == 38 || event.keyCode == 32 || event.keyCode == 87)) {
			this.startSong.stop();
			this.gameScene.addEventListener(this.onEvent);
			this.setScene(this.gameScene);
			this.startGame();
			this.startScene.removeChildren();
			this.startScene.remove();
			this.isStartScene = false;
		}
	}
}