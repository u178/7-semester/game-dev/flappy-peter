class Config {
    
    // Text settings
    public static var gameNameScaleFactor = 5;
    public static var startScreenScaleFactor = 3;
    public static var gameScoreScaleFactor = 2;

    // Game Gravitation
    public static var gameGravitation = 0.05;
    public static var tractorAcceleration = 2;

    // Obstacle Settings
    public static var minGapBetweenObstacles = 400;
    public static var maxGapBetweenObstacles = 700;
    public static var verticalGapBeforeObstacleGap = 50;


    // Player checkbox modifier
    public static var rightPlayerBorder = 5;
    public static var leftPlayerBorder = 10;
    public static var topPlayerBorder = 10;
    public static var bottomPlayerBorder = 0;


    // Difficulty Easy settings
    public static var ePipeGapHeight = 2.4;
    public static var eGameSpeed = 1.5;
    
    // Difficulty Normal settings
    public static var mPipeGapHeight = 2.2;
    public static var mGameSpeed = 1.5;

    // Difficulty Hard settings
    public static var hPipeGapHeight = 1.8;
    public static var hGameSpeed = 1.5;
}