import format.hl.Data.CodeFlag;

class Player {
    public var bm:h2d.Bitmap;
    public var brokenBm:h2d.Bitmap;

    var verticalSpeed: Float;
    public var y: Float;
    var iterations: Int;
    var iterationsDelayBeforeNextJump: Int = 8;

    var scaleFactor: Float = 0.25; 
    var x: Float;

    var acceleration: Float = 0;
    var speed: Float = 0;


    public function new(image, brokenImage, startingVerticalSpeed, scene, x:Float, y:Float) {
		this.bm = new h2d.Bitmap(image, scene);
        this.bm.scale(this.scaleFactor);
		this.bm.x = x;
		this.bm.y = y;
        this.y = y;
		this.verticalSpeed = startingVerticalSpeed;
        this.x = x;

        this.brokenBm = new h2d.Bitmap(brokenImage, scene);
        this.brokenBm.alpha = 1;
        this.brokenBm.scale(this.scaleFactor);
	}

	public function update(dt:Float) {
        this.iterations += 1;
        this.bm.y = this.y;
        this.verticalSpeed += this.acceleration;
        this.y -= verticalSpeed;
        this.acceleration = -Config.gameGravitation;
	}

    public function jump() {
        if (this.iterations >= this.iterationsDelayBeforeNextJump ) {
            this.iterations = 0;
            this.acceleration += Config.tractorAcceleration;
        }
    }

	public function toString() {
		return 'Peter(${this.y})';
	}

    public function checkPlayerInPlaybleArea() {
        
        if (this.y - this.scaleFactor * this.bm.height < 0) {
            return true;
        } else if (this.y + this.scaleFactor * this.bm.height > this.bm.getScene().height) {
            return true;
        }

        return false;
    }

    public function getHeight() {
        return this.bm.tile.height * this.scaleFactor;
    }

    public function getLeftX() {
        return this.x + Config.leftPlayerBorder;
    }

    public function getRightX() {
        return this.x + (this.bm.tile.width* this.scaleFactor) - Config.rightPlayerBorder;
    }

    public function getTopY() {
        return this.y + Config.topPlayerBorder;
    }

    public function getBottomY() {
        return this.y + (this.bm.tile.height* this.scaleFactor) - Config.bottomPlayerBorder;
    }

    public function restart(playerY) {
        this.verticalSpeed = 0;
        this.acceleration = 0;
        this.iterations = 0;
        this.bm.y = playerY;
        this.y = playerY;
        this.bm.alpha = 1;
        this.brokenBm.alpha = 0;
    }

    public function lose() {
        this.brokenBm.y = this.bm.y;
        this.brokenBm.x = this.bm.x;
        this.brokenBm.alpha = 1;
        this.bm.alpha = 0;
    }

    public function remove() {
        this.bm.remove();
    }

}